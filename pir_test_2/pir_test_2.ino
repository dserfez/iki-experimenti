#define PIR_JEDAN_PIN 14
#define PIR_DVA_PIN 15
#define PIR_STATUS HIGH

//#define jedan 
//#define dva 

void setup() {
  pinMode(PIR_JEDAN_PIN, INPUT);
  pinMode(PIR_DVA_PIN, INPUT);
  Serial.begin(115200);
  Serial.println("Palim se...");
}

void loop() {
  String jedan = "jedan: "+ digitalRead(PIR_JEDAN_PIN);
  String dva = "dva: "+ digitalRead(PIR_DVA_PIN);
  Serial.println(jedan);
  Serial.println(dva);
  delay(500);
}
