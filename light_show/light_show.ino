#include "FastLED.h"

#define NUM_LEDS 50
#define DATA_PIN 2
#define DELAY 20  // pauza između paljenja pojedinog LED segmenta
#define TRAJANJE 5000
#define PIR_JEDAN_PIN 14
#define PIR_DVA_PIN 15
#define PIR_STATUS LOW

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  FastLED.clear();
  pinMode(PIR_JEDAN_PIN, INPUT);
  pinMode(PIR_DVA_PIN, INPUT);
  Serial.begin(115200);
  Serial.println("Palim se...");
}

void loop() {
  if (digitalRead(PIR_JEDAN_PIN) == PIR_STATUS) {
    Serial.println("Palim smjer JEDAN");
    smjerJedan();
  } else if (digitalRead(PIR_DVA_PIN) == PIR_STATUS) {
    Serial.println("Palim smjer DVA");
    smjerDva();
  } else {
    Serial.println("Nista ne palim");
    delay(500);
  }
}

void smjerJedan() {
  FastLED.clear();
  for(int led = 0; led < NUM_LEDS; led++) {
    leds[led] = CRGB::White; FastLED.show(); delay(DELAY);
  }
  delay(TRAJANJE);
  for(int led = 0; led < NUM_LEDS; led++) {
    leds[led] = CRGB::Black; FastLED.show(); delay(DELAY);
  }
}

void smjerDva() {
  FastLED.clear();
  for(int led = NUM_LEDS; led >= 0; led--) {
    leds[led] = CRGB::White; FastLED.show(); delay(DELAY);
  }
  delay(TRAJANJE);
  for(int led = NUM_LEDS; led >= 0; led--) {
    leds[led] = CRGB::Black; FastLED.show(); delay(DELAY);
  }
}
